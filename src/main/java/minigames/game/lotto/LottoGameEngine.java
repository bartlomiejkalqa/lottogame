package minigames.game.lotto;

import java.util.Scanner;
import java.util.Set;

import lombok.AllArgsConstructor;
import minigames.game.lotto.input.LottoInputReceiver;
import minigames.game.lotto.logic.LottoHitNumberCalculator;
import minigames.game.lotto.logic.LottoRandomGenerator;
import minigames.model.GameResultInfo;

@AllArgsConstructor
public class LottoGameEngine {

    private final LottoInputReceiver lottoInputReceiver;
    private final LottoHitNumberCalculator lottoHitNumberCalculator;
    private final LottoRandomGenerator randomGenerator;
    private final Scanner scanner;

    public GameResultInfo getHitNumbers() {
        final Set<Integer> inputNumbers = lottoInputReceiver.getSixNumbers(scanner);
        final Set<Integer> randomNumbers = randomGenerator.getRandomSixNumbers();
        return lottoHitNumberCalculator.getHitNumbers(inputNumbers, randomNumbers);
    }
}
