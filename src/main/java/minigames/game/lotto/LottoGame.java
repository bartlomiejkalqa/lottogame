package minigames.game.lotto;

import lombok.AllArgsConstructor;
import minigames.game.Game;
import minigames.game.lotto.messageprovider.LottoMessageProvider;
import minigames.model.GameResult;
import minigames.model.GameResultInfo;

import static minigames.game.lotto.messageprovider.LottoMessageProvider.GAME_NAME;

@AllArgsConstructor
public class LottoGame implements Game {

    private final LottoGameEngine lottoGameEngine;

    public GameResult startGame() {
        System.out.println(String.format(LottoMessageProvider.GAME_S_STARTED, GAME_NAME));
        final GameResultInfo gameResultInfo = lottoGameEngine.getHitNumbers();
        return GameResult.createGameResult(this, gameResultInfo);
    }
}
