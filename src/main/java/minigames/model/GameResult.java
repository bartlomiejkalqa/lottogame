package minigames.model;

import minigames.game.Game;

public class GameResult {

    private final Game game;
    private final GameResultInfo gameResultInfo;

    public GameResult(Game game, GameResultInfo gameResultInfo) {
        this.game = game;
        this.gameResultInfo = gameResultInfo;
    }

    public static GameResult createGameResult(Game game, GameResultInfo gameResultInfo) {
        System.out.println(gameResultInfo.getGameResultMessage());
        return new GameResult(game, gameResultInfo);
    }

    public Game getGame() {
        return game;
    }

    public GameResultInfo getGameResultInfo() {
        return gameResultInfo;
    }
}
