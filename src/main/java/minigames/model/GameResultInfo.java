package minigames.model;

public interface GameResultInfo {

    String getGameResultMessage();
    default int getGameTime(){
        return 4;
    };
}
