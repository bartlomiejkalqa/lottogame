package minigames.game.lotto;

import minigames.model.GameResult;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class GameResultAssert {

    private final GameResult gameResult;

    GameResultAssert(GameResult gameResult) {
        this.gameResult = gameResult;
    }

    static GameResultAssert then(GameResult gameResult) {
        return new GameResultAssert(gameResult);
    }

    GameResultAssert hasCorrectGameResultMessage(String expectedMessage) {
        assertThat(gameResult.getGameResultInfo().getGameResultMessage()).isEqualTo(expectedMessage);
        return this;
    }

    GameResultAssert hasCorrectGameTime() {
        assertThat(gameResult.getGameResultInfo().getGameTime()).isEqualTo(4);
        return this;
    }
}