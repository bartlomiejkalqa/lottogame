package minigames.game.lotto.input;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class LottoInputReceiverTest implements SampleScanner {

    LottoInputReceiver lottoInputReceiver = new LottoInputReceiver();

    @Test
    void should_return_numbers_in_set_when_all_in_range_1_100() {
        // given
        Set<Integer> expectedNumbers = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6)));
        String givenNumbers = "1 2 3 4 5 101 6";
        Scanner userNumbers = createSampleScanner(givenNumbers);
        // when
        final Set<Integer> userInputNumbers = lottoInputReceiver.getSixNumbers(userNumbers);
        // then
        assertThat(userInputNumbers).isEqualTo(expectedNumbers);
    }

    @Test
    void should_return_0_numbers_in_set_when_all_not_in_range_1_100() {
        // given
        Set<Integer> expectedNumbers = Collections.emptySet();
        String givenNumbers = "0 -1 103 103 104 105 106";
        Scanner scanner = createSampleScanner(givenNumbers);
        // when
        final Set<Integer> userInputNumbers = lottoInputReceiver.getSixNumbers(scanner);
        // then
        assertThat(userInputNumbers).isEqualTo(expectedNumbers);
    }
}