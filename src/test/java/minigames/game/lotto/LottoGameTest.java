package minigames.game.lotto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Stream;

import minigames.game.Game;
import minigames.game.lotto.input.LottoInputReceiver;
import minigames.game.lotto.logic.LottoHitNumberCalculator;
import minigames.game.lotto.logic.LottoRandomGenerator;
import minigames.model.GameResult;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static java.util.Collections.unmodifiableSet;
import static minigames.game.lotto.GameResultAssert.then;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LottoGameTest {

    private static final Scanner scannerMock = new Scanner(System.in);
    private static final LottoHitNumberCalculator LOTTO_HIT_NUMBER_GENERATOR = new LottoHitNumberCalculator();
    private static final LottoInputReceiver lottoInputReceiverMock = mock(LottoInputReceiver.class);
    private static final LottoRandomGenerator randomGeneratorMock = mock(LottoRandomGenerator.class);
    private static final LottoGameEngine engine = new LottoGameEngine(lottoInputReceiverMock, LOTTO_HIT_NUMBER_GENERATOR, randomGeneratorMock, scannerMock);

    private static Stream<Arguments> provideNumbersAndMessages() {
        final Arguments hitSixNumbers = of(unmodifiableSet(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6))),
                unmodifiableSet(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6))),
                "You hit 6 numbers! Winning numbers were [1, 2, 3, 4, 5, 6], and yours were [1, 2, 3, 4, 5, 6]");

        final Arguments hitedZeroNumbers = of(unmodifiableSet(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6))),
                unmodifiableSet(new HashSet<>(Arrays.asList(7, 8, 9, 10, 11, 12))),
                "You hit 0 numbers! Winning numbers were [7, 8, 9, 10, 11, 12], and yours were [1, 2, 3, 4, 5, 6]");

        final Arguments hitOneNumber = of(unmodifiableSet(new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6))),
                unmodifiableSet(new HashSet<>(Arrays.asList(6, 8, 9, 10, 11, 12))),
                "You hit 1 numbers! Winning numbers were [6, 8, 9, 10, 11, 12], and yours were [1, 2, 3, 4, 5, 6]");

        return Stream.of(hitSixNumbers, hitedZeroNumbers, hitOneNumber);
    }

    @ParameterizedTest(name = "player gave {0}, random numbers were {1}, result {2}")
    @MethodSource("provideNumbersAndMessages")
    void shouldReturnCorrectMessageWhenParametersWereGiven(Set<Integer> playerGivenNumbers,
                                                           Set<Integer> randomNumbers,
                                                           String expectedMessage) {
        // given
        when(lottoInputReceiverMock.getSixNumbers(scannerMock)).thenReturn(playerGivenNumbers);
        when(randomGeneratorMock.getRandomSixNumbers()).thenReturn(randomNumbers);
        Game lottoGame = new LottoGame(engine);
        // when
        final GameResult gameResult = lottoGame.startGame();
        // then
        assertThat(gameResult.getGameResultInfo().getGameResultMessage()).isEqualTo(expectedMessage);
    }

    @ParameterizedTest(name = "player gave {0}, random numbers were {1}, result {2}")
    @MethodSource("provideNumbersAndMessages")
    void shouldReturnCorrectMessageWhenParametersWereGivenUsingAssertObjectPattern(Set<Integer> playerGivenNumbers,
                                                                                   Set<Integer> randomNumbers,
                                                                                   String expectedMessage) {
        // given
        when(lottoInputReceiverMock.getSixNumbers(scannerMock)).thenReturn(playerGivenNumbers);
        when(randomGeneratorMock.getRandomSixNumbers()).thenReturn(randomNumbers);
        Game lottoGame = new LottoGame(engine);
        // when
        final GameResult gameResult = lottoGame.startGame();
        // then
        then(gameResult)
                .hasCorrectGameResultMessage(expectedMessage)
                .hasCorrectGameTime();
    }
}